package br.com.itau.apiinvestimento.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;

@Entity
public class Simulacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nomeInteressado;

    @Email
    private String email;

    @Digits(integer=12,fraction=2)
    private double valorAplicado;

    private int quantidadeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento idInvestimento;


    public Simulacao() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(Investimento idInvestimento) {
        this.idInvestimento = idInvestimento;
    }
}
