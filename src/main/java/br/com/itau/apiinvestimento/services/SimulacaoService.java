package br.com.itau.apiinvestimento.services;

import br.com.itau.apiinvestimento.DTOs.RespostaSimulacaoDTO;
import br.com.itau.apiinvestimento.models.Investimento;
import br.com.itau.apiinvestimento.models.Simulacao;
import br.com.itau.apiinvestimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    public RespostaSimulacaoDTO salvarSimulacao(int idInvestimento, Simulacao simulacao){
        Investimento investimento = investimentoService.buscaPorId(idInvestimento);

        RespostaSimulacaoDTO respostaSimulacaoDTO = new RespostaSimulacaoDTO();
        respostaSimulacaoDTO.setRendimentoPorMes(investimento.getRendimentoAoMes());
        respostaSimulacaoDTO.setMontante(simulacao.getValorAplicado()*(Math.pow((1 + investimento.getRendimentoAoMes()), simulacao.getQuantidadeMeses())));

        simulacao.setIdInvestimento(investimento);
        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);

        return respostaSimulacaoDTO;
    }

    public Iterable<Simulacao> buscarTodasSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }
}
