package br.com.itau.apiinvestimento.services;

import br.com.itau.apiinvestimento.models.Investimento;
import br.com.itau.apiinvestimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Investimento buscaPorId(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        }
        throw new RuntimeException("O ID de Investimento informado não foi encontrado");
    }
}
