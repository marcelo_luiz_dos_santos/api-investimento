package br.com.itau.apiinvestimento.repositories;

import br.com.itau.apiinvestimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {
}
