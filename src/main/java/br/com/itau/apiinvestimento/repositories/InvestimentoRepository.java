package br.com.itau.apiinvestimento.repositories;

import br.com.itau.apiinvestimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
