package br.com.itau.apiinvestimento.controllers;

import br.com.itau.apiinvestimento.models.Investimento;
import br.com.itau.apiinvestimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento gravarInvestimento(@RequestBody @Valid Investimento investimento){
        return investimentoService.salvarInvestimento(investimento);
    }

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimentos = investimentoService.buscarTodosInvestimentos();
        return investimentos;
    }
}
