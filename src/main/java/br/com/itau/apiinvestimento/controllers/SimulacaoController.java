package br.com.itau.apiinvestimento.controllers;

import br.com.itau.apiinvestimento.DTOs.RespostaSimulacaoDTO;
import br.com.itau.apiinvestimento.models.Investimento;
import br.com.itau.apiinvestimento.models.Simulacao;
import br.com.itau.apiinvestimento.services.InvestimentoService;
import br.com.itau.apiinvestimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping("/investimentos/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaSimulacaoDTO gravarSimulacao(@PathVariable(name = "idInvestimento") int idInvestimento, @RequestBody @Valid Simulacao simulacao){

        RespostaSimulacaoDTO respostaSimulacaoDTO = simulacaoService.salvarSimulacao(idInvestimento, simulacao);

        return respostaSimulacaoDTO;
    }

    @GetMapping("/simulacoes")
    public Iterable<Simulacao> buscarTodasSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoService.buscarTodasSimulacoes();
        return simulacoes;
    }
}
