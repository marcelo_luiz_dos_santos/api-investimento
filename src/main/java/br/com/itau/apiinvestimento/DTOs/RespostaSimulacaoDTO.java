package br.com.itau.apiinvestimento.DTOs;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class RespostaSimulacaoDTO {
    private double rendimentoPorMes;

    private double montante;

    public RespostaSimulacaoDTO() {}

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }
}
